import React, { useState } from 'react';
import { StatusBar, StyleSheet, Text, View, Pressable, FlatList, TextInput, ImageBackground, Modal } from 'react-native';

export default function App() {
  const [sampleGoals, setNouvelObjectif] = useState([
    { id: '1', text: 'Faire les courses', completed: false },
    { id: '2', text: 'Aller a la salle de sport', completed: false },
    { id: '3', text: 'Apprendre React Native', completed: false },
  ]);
  const [enteredGoal, setEnteredGoal] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [itemToDelete, setItemToDelete] = useState(null);

  const goalInputHandler = (enteredText) => {
    setEnteredGoal(enteredText);
  };

  const addGoalHandler = () => {
    if (enteredGoal.trim() !== '') {
      setNouvelObjectif([...sampleGoals, { id: Date.now().toString(), text: enteredGoal, completed: false }]);
      setEnteredGoal('');
    }
  };

  const promptDelete = (index) => {
    setItemToDelete(index);
    setModalVisible(true);
  };

  const handleDelete = () => {
    const newGoals = sampleGoals.filter((_, goalIndex) => goalIndex !== itemToDelete);
    setNouvelObjectif(newGoals);
    setModalVisible(false);
    setItemToDelete(null);
  };

  const handleCancelDelete = () => {
    setModalVisible(false);
    setItemToDelete(null);
  };

  const handleDone = (item, index) => {
    const newGoals = sampleGoals.map((goal, goalIndex) => {
      if (goalIndex === index) {
        return { ...goal, completed: !goal.completed };
      }
      return goal;
    });

    setNouvelObjectif(newGoals);
  };

  return (
      <ImageBackground source={require('./assets/bg.jpg')} style={styles.backgroundImage}>
        <View style={styles.overlay}>
          <View style={styles.container}>
            <Text style={styles.boldText}>StayFit.App</Text>

            <View style={styles.inputContainer}>
              <TextInput
                  placeholder="Ajouter une tâche"
                  style={styles.input}
                  onChangeText={goalInputHandler}
                  value={enteredGoal}
              />

              <Pressable style={styles.buttons} onPress={addGoalHandler}>
                <Text style={{ fontWeight: 'bold' }}>Ajouter</Text>
              </Pressable>
            </View>

            <View style={styles.listContainer}>
              <FlatList
                  style={styles.listeObjectif}
                  data={sampleGoals}
                  renderItem={({ item, index }) => (
                      <View style={[styles.listItem, { borderColor: item.completed ? 'green' : 'red' }]}>
                        <Text>{item.text}</Text>

                        <Pressable style={styles.buttons} onPress={() => handleDone(item, index)}>
                          <Text>{item.completed ? 'Fait' : 'Non Fait'}</Text>
                        </Pressable>

                        <Pressable style={styles.deleteButton} onPress={() => promptDelete(index)}>
                          <Text>Supprimer</Text>
                        </Pressable>
                      </View>
                  )}
                  keyExtractor={(item, index) => index.toString()}
              />
            </View>

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                  setModalVisible(!modalVisible);
                }}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>Etes vous sur de vouloir supprimer cet objectif ?</Text>
                  <Pressable
                      style={[styles.button, styles.buttonYes]}
                      onPress={handleDelete}
                  >
                    <Text>Oui</Text>
                  </Pressable>
                  <Pressable
                      style={[styles.button, styles.buttonNo]}
                      onPress={handleCancelDelete}
                  >
                    <Text>Non</Text>
                  </Pressable>
                </View>
              </View>
            </Modal>

            <StatusBar style="auto" />
          </View>
        </View>
      </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  backgroundImage: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.5)',
  },
  boldText: {
    color: '#FF0000',
    fontWeight: 'bold',
    marginBottom: 20,
  },
  listeObjectif: {
    display: 'flex',
    height: '100%'
  },
  listContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  input: {
    width: '70%',
    borderColor: '#ddd',
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
    marginRight: 10,
    fontSize: 16,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 10,
    backgroundColor: '#f8f8f8',
    padding: 10,
    borderRadius: 12,
    borderWidth: 4,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },
  buttons: {
    padding: 10,
    backgroundColor: 'lightblue',
    borderRadius: 5,
    elevation: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '28%',

  },
  deleteButton: {
    padding: 10,
    backgroundColor: 'red',
    borderRadius: 5,
    elevation: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '28%',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  buttonYes: {
    backgroundColor: '#2196F3',
    marginTop: 10,
    marginBottom: 10,
  },
  buttonNo: {
    backgroundColor: '#f32121',
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});